﻿using System;

namespace LargestNumber
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(largestNumber(2));
        }

        static int largestNumber(int n) {
            string newN = "";
            for (int i = 0; i < n; i++)
            {
                newN += Convert.ToString(9);
            }
            return Convert.ToInt32(newN);
        }
    }
}
